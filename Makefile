Rulebook.pdf: Rulebook.md pdf/Rulebook-header.tex pdf/Rulebook-title.tex
	pandoc $< \
	--include-in-header pdf/Rulebook-header.tex \
	--include-before-body pdf/Rulebook-title.tex \
	--chapters -V documentclass=report -o $@
