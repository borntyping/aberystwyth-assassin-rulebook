The Aberystwyth Assassin Rulebook
=================================

[![Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

This repository contains a set of rules for the game 'Assassin', as run by the Aberystwyth Assassin organisation.

Published versions of the rulebook can be downloaded here: [Aberystwyth Assassin rulebook](https://github.com/aberassassin/rulebook/releases).

Assassin is a live-action game involving the elimination of other players using mock weapons.
For more information, see the following Wikipedia articles:

* [Assassin (game)](https://en.wikipedia.org/wiki/Assassin_%28game%29)
* [Live action role-playing game](https://en.wikipedia.org/wiki/Live_action_role-playing_game)
* [Humans vs. Zombies](https://en.wikipedia.org/wiki/Humans_vs._Zombies)

Publishing
----------

The main text of the rulebook is contained in `Rulebook.md`, and is in [Markdown](http://daringfireball.net/projects/markdown/) format. In addition to this, the title page used for the PDF is in `Rulebook-title.tex`, and some configuration for the PDF is is `Rulebook-header.text` - both are in [LaTeX](http://www.latex-project.org/) format, and can usually be safely ignored.

### Windows

To create a PDF of the rulebook, you will need to install [Pandoc](http://johnmacfarlane.net/pandoc/index.html) and [MikTex](http://miktex.org/). Once those are installed, run the `build` script in this folder.

History
-------

The current rulebook is maintained and published by Sam Clements ([@borntyping](https://github.com/borntyping)).
It has been adapted by the original Aberystwyth Assassin rules, which were written by Robert Grayston.
In turn, those rules were inspired by Steve Jacksons '[Killer](http://www.sjgames.com/killer/)'.

Contributors
------------

The following people have contributed to these rules:

* Robert Grayston
* Sam Clements ([github](https://github.com/borntyping), [email](sam@borntyping.co.uk))
* Yarrow Paddon-Butler ([github](https://github.com/Goggles))
* Jay Appleseed.
* Matt Calfe
* John Kedzierski
* Ben Mayne

Licence
-------

This work is licensed under the **[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/)**.

This allows you to share and adapt this work, however:

* You must give appropriate credit, provide a link to the license, and indicate if changes were made.
* You may not use the material for commercial purposes.
* If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
