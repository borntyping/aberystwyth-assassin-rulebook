About Assassin
==============

## What is Assassin?

Assassin is a lifestyle-invasive game where you eliminate other players using a variety of non-harmful weaponry. It is traditionally played as a non-team game, but includes variants that involve playing as a team and objectives beyond the elimination of opponent players.

Potentially anybody, anywhere could be an Assassin waiting to pounce.
You may become paranoid and distrustful of strangers, or even your own friends.
They may just be lulling you into a false sense of security before they strike.

While playing Assassin, you are never safe, and must be constantly prepared for anything to happen.

## Participating in Assassin

**While Assassins are encouraged to be inventive and bold, we also encourage them to sensible, safe and responsible.** Players that do not do this will not be able to participate in future games.

If you are in doubt about anything, do not hesitate to get in touch with the Administrator - they cannot accept any responsibility for loss or injury during a game, as most events will be out of their control and in the hands of those Assassins who are playing. You should also bear in mind that Assassin usually involves the distribution of some of your personal details for the purposes of the game.

Assassin is run by a informal group of staff with no affiliation with any other organisation. **When playing, you should be prepared to follow both the rules described in this book, and the instructions of any staff.**

## Contacting the Administrator

The current Administrator can be contacted at [assassinaber@gmail.com](mailto:assassinaber@gmail.com), as of the most recent update of this document. The community Facebook page is available at [facebook.com/aberassassin](https://www.facebook.com/aberassassin). Historically, there has also been an online forum available at [aberassassin.proboards.com](http://aberassassin.proboards.com).



The Rules
=========

## The Staff

There are several staff roles that manage Assassin events, described below.
Unless explicitly described otherwise, **staff members are not considered players and cannot be eliminated**.

It is important to bear in mind that often the Administrator and other staff will often spend time with Assassins. While most staff have a pretence of honour, they are under no restrictions as to the information they can share with Assassins or other people.

The Administrator

:   Each game of Assassin is run by a single _Administrator_. Their role is to manage the game from start to finish - they will resolve conflicts, establish scenarios and generally try to provide a flexible framework in which the Assassins can have an enjoyable experience. The Administrator is not a player, and cannot be eliminated.

Vice-Administrators

:   Vice-Administrators assist the Administrator, and have exactly the same responsibilities and powers. However, the Administrator has the final say in any decisions.

Assistants

:   From time to time, the Administrators may bring in additional staff. Rules for additional staff are either defined in the game type being played, or announced ahead of time by the Administrators.

## Restrictions

As mentioned in the introduction, you are expected to play in a safe, sensible and responsible manner. Players who don't abide the rules - especially those relating to safety and public disturbance - will be removed from the game, and will not be able to join future games.

### Off-limit areas

There are certain areas that are always off-limits for Assassination attempts, because they would be disrespectful, disturbing to others, or cause other such problems. No Assassination attempts may happen in or from these places. Forced entry, trespassing and any other illegal activities are not allowed by the game -- please remember this.

While it might look like a lot of places to remember, most of them common sense, and Assassins able to apply common sense should not have any problems.
As well as these places, please be sensible with your eliminations and bear in mind that interrupting or disturbing the public may cause results beyond the game.

- Examination rooms, or any areas within earshot of an ongoing examination
- Lecture and seminar rooms whilst a lecture or seminar is in progress
- Hospitals, surgeries, dentists and other medical facilities
- Churches, chapels, and other places of worship
- Banks, building societies and other financial institutions
- The accommodation of the current Administrator and any Vice-Administrators, unless they declare otherwise

An Assassin is considered out of play, and may not be eliminated nor eliminate whilst:

* In any small vehicle, or any vehicle they are in control of
* At work, including both formal and volunteer work
* Taking part in a sporting event

In addition to the above, certain areas are out of play on a temporary basis:

- Any area declared off-limits by an Administrator
- The location of a duel (See [Duels])
- The vicinity of a _Beret of Peace_ worn by an Administrator

The _Beret of Peace_ is a beret worn by an Administrator to indicate a brief termination of hostilities within sight of the Beret. It's mostly worn for duels and socials.

### Interaction with non-players

Assassins should aim to be stealthy and professional. Please be sensible and try not to panic people, or cause public disturbances by doing things such as running through the streets of Aberystwyth in a gas mask, or throwing a `grenade' into a society event[^dontdothis].

[^dontdothis]: Both of these things have actually happened in past games, and are not recommended.

While interaction with non-players is inevitable, they are not part of the game, and incidents that involve them ending up in the line of fire may result in your removal from the game, or Agents being sent after you, depending on severity. Using non-players to willingly interact with the game - for example, having them disarm traps, physically interfere with other players, or having them intentionally attempting to eliminate players - is not allowed. Assassins should be fully capable of achieving their goals without the help of mere mortals.

Non-players who wish to take part in the game should contact the Administrator - new players can often be introduced to a game when an Assassin is eliminated and new targets are assigned.

However, it is perfectly acceptable to use non-players as the eyes and ears of Assassins - good Assassins often have a network of contacts they can use to gain information -- but beware of double-crosses or extortion on the part of the non-player. You may wish to ask any non-players you live with to inform you of any unusual activity around your accommodation, or to not let any strangers carrying an unusual number of bananas in -- just hope they haven't been bribed.

## Elimination

[The Armoury] lists some of the the many suggested methods of elimination. These basic choices are always available, but if you think up something particularly imaginative or original, ask an Administrator for approval, and you may well be allowed to use it. It always permissible to eliminate or attempt to eliminate another Assassin, though some game types may discourage indiscriminate eliminations.

Assassins are not immune to their own weapons -- if you drink from the wrong glass, set off one of your own traps, or end up shooting yourself, you've eliminated yourself. Please inform the Administrator or Vice-Administrators of all traps you place/discover/disarm -- it makes cleaning up after a game much easier.

**Do not use any weapon from the following categories** -- you will be immediately removed from the game, with no repeal.

- Can be mistaken for a real weapon. The Anti-social Behaviour Act of 2003 prohibits possession of an imitation weapon in public as an offence.
- Can cause injury -- while many players may have different pain thresholds, anything that will cause bruising or physical damage is not allowed.
- Can leave a permanent stain or do damage to property.
- Will damage electrical equipment such as hearing aids, phones and laptops

Finally, and quite importantly, whilst there may be times when physical contact is made, Assassin is mostly a non-contact game. If things become heated, Assassins should ask each other for a gentlemanly "time-out" to prevent any violent physicality. Grappling and brawling is in no way approved of, and will result in immediate removal from the game.

## Being eliminated by other players

> So you weren't fast enough, you didn't check that drawer, or it didn't occur to you that a friend wanting to meet up by the fruit section of Morrison's was a bit odd. -- _Robert Grayston, Aberystwyth Assassin Founder_

**You should inform an Administrator as soon as possible when you are eliminated** - after a suitably dramatic death[^bestdeath]. Include details of the time, place and cause. The Administrators are the final judge of whether an Assassin has been eliminated or not. Eliminated players are no longer considered Assassins, but they are still considered to be players.

Once you have been eliminated, you are no longer allowed to participate in the game - in particular, you are not permitted to discuss any information relating to the current game. After all, dead men tell no tales!

However, if there were any witnesses, they may freely talk about who the Assassin was, and can completely blow their cover, which is a good reason to not attempt Assassinations in public places.

[^bestdeath]: A `best death' award is a long-standing tradition of the game.

## Duelling

Duels are an extension to the rules that allow two or more Assassins to formally challenge each other to a contest, with the loser(s) being eliminated. They can be used in any game, unless announced otherwise.

In a game such as Assassin, honour and martial pride can be important for many players. Duels can be an easier way to get hold of your target if you feel confident enough in a win, or as a desperate do-or-die last-chance. However, duels have certain regulations which must be observed.

Any method of duelling that can be agreed upon between the two Assassins and the Administrator is acceptable -- previous duels have included Nerf blasters, Chess and Pool. Duels may be issued in any form, and accepted or rejected in the same way. Accepted duels should be accompanied by a notification to the Administrator.

An independent person must be present as the duel's referee, in order to observe the proper conduct and outcome. In most cases this should be an Administrator, but as an alternative they should appoint an impartial referee.The area of a duel is out of play for the duration of the duel, starting and finishing when the referee declares. The duellists may only attack each other within the rules of the duel.

## Hints, tips and gamesmanship

> Not a crazed gunman, Dad, I'm an assassin! Well, the difference being one's a job, and the other's mental sickness! -- _Sniper, Team Fortress 2_

Gamesmanship is a key element of Assassin -- a lot of the game is very difficult to enforce. If you do get yourself with one of your own traps, the Administrator has absolutely no way of knowing, and so it is left to your own sense of honour. Traps and certain poisons are also difficult to observe in effect, but if you have been eliminated, then you should report it. After all, there will be other games in which to enact revenge, or perfect your room bomb-clearing procedure! Playing a scenario with a sense of honour and good gamesmanship is the best way to be invited to the next game.

Assassination attempts should be - to some extent - "fair", if the target is somewhat paranoid/careful/clever enough. Naturally, some Assassins would rather a certain kill than any chance of a victim escaping, and this is an entirely individual decision.

Should players so wish, they may booby-trap their own rooms (but inform the Administrators as with all traps), or even, if the Assassin is so technologically-minded, set up some form of alarm or even a camera that can photograph intruders! Bond-esque attachment of hairs to door-frames, stretching out thin lines of thread, and other such means of telling whether a room has been entered are also all good ideas.

If double-crossing, be sure to get something good out of it -- the price should be suitably high for you to betray your own team (and to counter their inevitable annoyance -- two drinks at least seems fair, and has a significant precedent from multiple past games). When in a scenario with double-crossing, always make sure to check your weapons -- who knows who may have tampered with them?  As well as this, anyone may attempt to coerce information from the staff, but they should keep in mind that a) we're not cheap and b) we lie.

Lastly, if you do find yourself in a corner, the hand-buzzer's failed, you dropped the ammo for your spud pistol, and there's an angered target now after you with his concealed Nerf pistol and back-up water pistol... try talking your way out. You might pretend to have a banana in your pocket, you might keep them talking just long enough to get a text off about their identity or for help, or you might even be able to strike a deal. But remember, never say die![^neversaydie]

[^neversaydie]: It's not nearly dramatic enough; you'll never win a "best death" award like that.



Game types
==========

## Circle of Death

Every Assassin is assigned a target, and is also assigned as another Assassin's target, creating a "circle" of Assassins. Once an Assassin is eliminated, their target is given to their Assassin.

During Circle of Death games, it is acceptable to eliminate Assassins when they are your assigned target, or when they are holding a weapon or placing a trap, poison or other such weapon. Once the weapon is put away or hidden, or they have completed placing the weapon, or have otherwise disposed of it (for instance, by throwing it across the room) they are no longer a target.

## Godfather

This is a team game, where several Mafia-style "families" attempt to kill the Godfathers of the opposing teams. At the start, every team chooses a team name and a Godfather to act as their leader. Each Godfather's first act is to elect a successor, who will take their place as Godfather should they be eliminated. The Godfather of every team is always publicly identified, along with three other team members chosen by the Godfather at the start of the game. Beyond this, the rest of the team will be unknown.

Elimination may be undertaken of any known or suspected Assassins. This is not _carte blanche_ to eliminate everyone you come across -- abuse of this freedom will not be tolerated by the staff. The team that has eliminated the most Godfathers at the end of the game is declared the winner, with all other eliminations taken into account in the event of a tie.

## Secret War

This is a team game, played with any number of teams. Any member of another team is a valid target, with each elimination scoring a point for the team. Bonus points may also be assigned for style, form or inventiveness, depending on the game.

Players respawn at 6AM each day, but once a player has eliminated another player, or been eliminated by them once, those two players can no-longer eliminate each other. Once eliminated, a player can't pass on information until they respawn.

<!-- ## VIP Escort

This is an asymmetric game, usually played in a single day. A defending team attempts to escort a VIP between two locations, while an attacking team attempts to eliminate the VIP.

## Bourne

One Assassin starts in the position of Bourne. Once eliminated their Assassin adopts the position of Bourne -- this continues until the end of the game, with the current Bourne declared the winner. -->



The Armoury
===========

The Armoury is a list of suggested and endorsed mock weapons for use in Assassin games. You are encouraged to come up with your own ideas, but must check with the games Administrator before using them.

## Ballistic weapons

> "Some people think they can outsmart me. Maybe... maybe. I've yet to meet one that can outsmart bullet." -- _Heavy, Team Fortress 2_

Banananananana gun

:   The safest of all Assassination methods. Take a banana, stand next to your victim, point it at them and shout ``Bang!'' If you botch taking out the banana and it gets squashed for any reason, you may not attempt the Assassination. You may still consume the banana. The range is arm's length, which may require a judgement call on behalf of both the Assassin and the victim.

Water gun

:   Potentially easy to conceal and quiet. It also does away with arguments on whether the shots hit or not -- the water stains. Be sure not to use this on people carrying electric equipment.

Water balloon

:   This method is simple -- fill a balloon with water, and throw it at the target. If they only receive a small amount of splashback, they are assumed to be OK, but a large damp patch is indicative of a kill.

Nerf guns

:   Nerf-style weaponry is an ideal Assassin's weapon -- good range, pistols are easy to conceal, can carry a reasonable supply of ammunition, and just that little bit cooler than a lot of the other suggestions presented here. They can also be used in the same way as bananas, by placing the weapon against the person (it must be touching or almost touching them) and saying "BANG!" (so as to avoid shooting them at point blank).

Bean bag grenades

:   As an alternative to Nerf guns, _Humans vs. Zombies_-style rolled up socks or soft bean bags can be used as a thrown weapon. However, they cannot be used at point blank.

Sniper camera

:   A red-dot laser-pointer is shined at the target, and this is photographed as the eliminating shot. This photo is then sent to the Administrator to confirm the elimination.

## Poison

> "A dram of poison, such soon-speeding gear
>
> As will disperse itself through all the veins"
>
> -- William Shakespeare, Romeo and Juliet


Poison notes

:   Write the word "poison" on a piece of paper, and affix it to the bottom of the target's plate or mug. Once they eat or drink from it, they are eliminated. If you wish to play mind games with your target, replacing "poison" with all sorts of other taunting messages is suitably evil.

Hand buzzer

:   A cheesy classic. Get the target to shake hands with you, as soon as they're buzzed you've scored a kill, unless they're wearing gloves. Shaking hands is the only method acceptable with a buzzer -- you cannot shove it in a target's face or hit them with it.

Anthrax envelope

:   Fill an envelope with a bit of glitter or cake sprinkles (a heaped teaspoon's worth at least and definitely not sugar, salt or flour or anything else white), then post it off to your target, who will open it and then be eliminated. You may write a letter inside to inform them of their eliminated status. As a form of psychological warfare, you could not fill it with glitter or cake sprinkles but still have a letter inside informing them how easy it could have been to eliminate them.

Contact poison

:   Vaseline or flavoured lipstick. Put some on a door knob, a drawer handle, or coat a glove in it and shake hands with someone. As long as it touches bare skin, the target is eliminated.

Poisoned lipstick

:   Flavoured lipstick can be used as a contact poison, but does not harm the user as long as it only touches their lips. Before you ask - yes, this does get used.

## Traps

> "What makes me a good Demoman? If I were a bad Demoman, I wouldn't be sitting here, discussing it with you, now would I?!" -- _Demoman - Team Fortress 2_

Activated bomb traps will blow up an entire room, eliminating everyone in that area unless they are protected by a wall (but not glass or plastic, such as a shower door).

Spiked pit

:   To do this involves simply taking a piece of paper and drawing lots of spikes, and writing ``spiked pit'' on it. This is then turned upside down and placed anywhere at ground level (think about it logically). Grass/rugs/etc may be placed over it as a disguise, but it will be easy to spot, and it is best the Assassin is present to claim their elimination.

Pin and balloon

:	This is among the simplest of all traps -- blow up a balloon, and set up a pin so that the victim will cause the balloon to be burst. Doors, drawers, windows... wherever you think it will work. Whoever sets it off is eliminated.

Alarm clock bomb

:	The `'bomb'' must be hidden in a shoe-box or similarly-sized container. Simply wind up an alarm clock, or set an electric device -- once it goes off, if the victim is in the room, they're eliminated! An accompanying note to inform them of the clock's bomb status is advised.

Recorded bomb

:	Record a message onto a CD or tape, perhaps with a blank period up until the actual "explosion". After a few minutes have elapsed, the recording should then say "BOOM!", with a possible message of apology from the Assassin. An alternative version on this is where there is no blank period, but instead a recording with a countdown - "I'm a bomb and will explode in twenty... nineteen... eighteen..." This version will probably not eliminate the victim unless they are locked in with the device, or the Assassin is sneaky and lies about the timer (e.g. "nineteen... eighteen... BOOM!"").

Screensaver bomb

:	Simple -- get on the target's computer, adjust their screensaver appropriately and wait for them to leave it idle for a while.

Cluster Bomb

:	Fold a piece of paper multiple times, as tight as you can, then write "boom" on several of the folds (ideally interior ones the target won't see straight away). Should your target be curious enough to open a piece of paper they found lying around, they have been blown up.

Book Bomb

:	Place a post-it note where the target will see it in a book. When they open it... Boom!

Whoopee Cushion

:	This one is fairly self-explanatory. If someone sits/otherwise triggers it, they have been eliminated.

## Miscellaneous

> "Look, look, look, Mr Knowall. The 16-ton weight is just _one way_ of dealing with a raspberry killer. There are millions of others!" -- _Self-Defence Against Fresh Fruit - Monty Python_

Sixteen ton weight

:	Write a note that says `sixteen ton weight', attach it to a pillow and then drop it on your target. An alternative to this is a large amount of balloons -- silent and very funny as they slowly descend upon an unaware target! This must be done from a first or second-storey window.

Umbrellas

:	A special case. The rare and majestic umbrella is well-designed to stop the shots of would-be Assassins. An umbrella may be used as a shield and anything striking it instead of those shielded by it will be discounted, including directing an umbrella towards an explosive. Umbrellas may not be used indoors (awfully unlucky, don't you know!).

Burning down the house

:	The most ambitious of all Assassination methods, this requires that the all accessible ground floor windows of a target's house be covered in "flame coloured" material (usually paper). 'Covered' in this context means that the majority of the openings into the building are obscured.

Glow sticks

:	These act as radioactive devices. Simply place three or more under a bed, and once the target awakes they will have been eliminated.
